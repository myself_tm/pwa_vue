import store from '@/store'
// import auth from '@/auth/helpers'

export default class Service {
  constructor (options) {
    this.id = store.state.auth.user.id
  }

  getProfile () {
    // Mock data.
    // Replace this with actual call to backend server below.
    const parsed = {
      email: 'phoenix.number377@gmail.com',
      name: 'Phoenix',
      country: 'USA',
      addressLine1: '1234 Some St.',
      addressLine2: '',
      state: 'Texas',
      zipcode: '78789'
    }

    // Simulate loading time.
    return new Promise((resolve) => {
      setTimeout(() => { resolve(parsed) }, 500)
    })
  }
}
